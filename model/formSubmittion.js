const mongoose = require('mongoose');

const FormSubmittionSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true,
        unique: true
    },
    phone:{
        type: Number,
        require: true,
        unique: true,
    },
    message: {
        type:String,
        require: true
    }
},
{
    timestamps: true
});


const formSchema = mongoose.model('FormSubmittion' , FormSubmittionSchema);
module.exports = formSchema;