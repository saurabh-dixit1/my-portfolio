const express = require("express");
const FormSubmittion = require("./model/formSubmittion");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();
const nodemailer = require("nodemailer");
const PORT = 8000;
const app = express();

// DataBase Connection
const connectDB = async () => {
  const connectionString = process.env.MONGO_URL;

  let isConnected = false;

  while (!isConnected) {
    try {
      const con = await mongoose.connect(connectionString);
      const isConntected = mongoose.connection.readyState === 1;
      if (isConntected) {
        console.log(
          `MongoDB Database is connected Successfully and Database connection is active on :  ${con.connection.host}` // install colors for use this
        );
      } else {
        console.log("Database connection is not active");
      }
      isConnected = true;
    } catch (error) {
      console.error("MongoDB connection error:", error);
      console.log("Retrying connection in 5 seconds...");
      await new Promise((resolve) => setTimeout(resolve, 5000)); // 5 seconds delay
    }
  }
};

// nodemailer transporter
const transporter = nodemailer.createTransport({
  host: process.env.NODEMAILER_HOST,
  port: process.env.NODEMAILER_PORT,
  auth: {
    user: process.env.NODEMAILER_USERNAME,
    pass: process.env.NODEMAILER_PASSWORD,
  },
  secure: process.env.NODEMAILER_SECURE,
});

// url endcoding
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static("assests"));

app.get("/", (req, res) => {
  return res.sendFile(__dirname + "/views/index.html");
});

app.post("/submit-form", async (req, res) => {
  try {
    const { name, email, phone, message } = req.body;
    const formSubmit = await FormSubmittion.create({
      name: name,
      email: email,
      phone: phone,
      message: message,
    });
    formSubmit.save();

    // email notification
    const mailOption = {
      from: email,
      to: process.env.NODEMAILER_TO,
      subject: "New Form Submittion ",
      text: `Name: ${name}\nEmail: ${email}\nPhone: ${phone}\nMessage: ${message}`,
    };
    transporter.sendMail(mailOption, (error, information) => {
      if (error) {
        return res.redirect("back");
        return;
      } else {
        return res.redirect("back");
      }
    });
  } catch (error) {
    return;
  }
});

app.use("*", (req, res) => {
  return res.sendFile(__dirname + "/views/404.html");
});

// for production mode
connectDB().then(() => {
  app.listen(PORT, () => {
    console.log(`Successfull Connected With the Port:${PORT}`);
  });
});
